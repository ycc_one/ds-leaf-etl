package com.leaf.etl.entity;

import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author：ycc
 * @since： 2016/12/13
 */
public class SocialUser {

    //twitter_en_v1_user fields
    private String id;
    private String nickname;
    private List<String> history_nickname;
    private String screen_name;
    private String profile_image_url;
    private String lang;
    private Date created_at;
    private String time_zone;
    private Boolean protected_user;
    private String description;
    private Long statuses_count;
    private Long friends_count;
    private Long followers_count;
    private Long likes_count;
    private Boolean verified;
    private String location;
    private String location_country;
    private String url;
    private Long uptime;
    private Boolean is_rolling_update;
    private Long ftime;
//    private List<SocialUser.FollowersDistItem> followers_dist;
    private String utc_offset;
//    private List<SocialUser.Friend> friends;

    //twitter_en_v1_user_ex1 fields
    private Double a_liveness;
    private Double a_influence;

    //facebook 多出的字段
    private String email;
    private String gender;
    private String birthday;
    private Short birthyear;
    private String college;
    private String city;
    private Short page_type;
    private Map<String, String> rooms;
    private List list;


//    /**
//     * 转换 es的search hit 到具体的对象 TwUser 中
//     *
//     * @return SocialTwitterUser
//     * @throws IllegalAccessException
//     */
//    public static SocialUser fromHit(SearchHit hit) throws IllegalAccessException, ParseException, InstantiationException, ClassNotFoundException {
//        Map<String, Object> source = hit.getSource();
//
//        // inner_hits
//        if (hit.getInnerHits() != null) {
//            Map<String, SearchHits> innerHits = hit.getInnerHits();
//
//            SearchHits _hit = null;
//            if (innerHits.containsKey(ES_TYPE_SKYNET_SOCIAL_TWITTER_USER_EX)) {
//                // user -> user_ex1
//                _hit = innerHits.get(ES_TYPE_SKYNET_SOCIAL_TWITTER_USER_EX);
//            } else if (innerHits.containsKey(ES_TYPE_SKYNET_SOCIAL_TWITTER_USER)) {
//                // user_ex1 -> user
//                _hit = innerHits.get(ES_TYPE_SKYNET_SOCIAL_TWITTER_USER);
//            }
//            if (_hit != null) {
//                SearchHit[] _hits = _hit.getHits();
//                if (_hits != null && _hits.length > 0) {
//                    Map<String, Object> _source = _hits[0].getSource();
//                    source.putAll(_source);
//                }
//            }
//        }
//
//        return (SocialUser) SearchWrapUtil.fromSourceMap(source, SocialUser.class);
//    }
//
//    public static List<SocialUser> fromHit(SearchHit[] hits) throws IllegalAccessException, ParseException, InstantiationException, ClassNotFoundException {
//        List<SocialUser> users = new LinkedList<>();
//        for (SearchHit hit : hits) {
//            SocialUser user = SocialUser.fromHit(hit);
//            users.add(user);
//        }
//        return users;
//    }
//

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getNickname() {
//        return nickname;
//    }
//
//    public void setNickname(String nickname) {
//        this.nickname = nickname;
//    }
//
//    public List<String> getHistory_nickname() {
//        return history_nickname;
//    }
//
//    public void setHistory_nickname(List<String> history_nickname) {
//        this.history_nickname = history_nickname;
//    }
//
//    public String getScreen_name() {
//        return screen_name;
//    }
//
//    public void setScreen_name(String screen_name) {
//        this.screen_name = screen_name;
//    }
//
//    public String getProfile_image_url() {
//        return profile_image_url;
//    }
//
//    public void setProfile_image_url(String profile_image_url) {
//        this.profile_image_url = profile_image_url;
//    }
//
//    public String getLang() {
//        return lang;
//    }
//
//    public void setLang(String lang) {
//        this.lang = lang;
//    }
//
//    public Date getCreated_at() {
//        return created_at;
//    }
//
//    public void setCreated_at(Date created_at) {
//        this.created_at = created_at;
//    }
//
//    public String getTime_zone() {
//        return time_zone;
//    }
//
//    public void setTime_zone(String time_zone) {
//        this.time_zone = time_zone;
//    }
//
//    public Boolean getProtected_user() {
//        return protected_user;
//    }
//
//    public void setProtected_user(Boolean protected_user) {
//        this.protected_user = protected_user;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public Long getStatuses_count() {
//        return statuses_count;
//    }
//
//    public void setStatuses_count(Long statuses_count) {
//        this.statuses_count = statuses_count;
//    }
//
//    public Long getFriends_count() {
//        return friends_count;
//    }
//
//    public void setFriends_count(Long friends_count) {
//        this.friends_count = friends_count;
//    }
//
//    public Long getFollowers_count() {
//        return followers_count;
//    }
//
//    public void setFollowers_count(Long followers_count) {
//        this.followers_count = followers_count;
//    }
//
//    public Long getLikes_count() {
//        return likes_count;
//    }
//
//    public void setLikes_count(Long likes_count) {
//        this.likes_count = likes_count;
//    }
//
//    public Boolean getVerified() {
//        return verified;
//    }
//
//    public void setVerified(Boolean verified) {
//        this.verified = verified;
//    }
//
//    public String getLocation() {
//        return location;
//    }
//
//    public void setLocation(String location) {
//        this.location = location;
//    }
//
//    public String getUrl() {
//        return url;
//    }
//
//    public void setUrl(String url) {
//        this.url = url;
//    }
//
//    public Long getUptime() {
//        return uptime;
//    }
//
//    public void setUptime(Long uptime) {
//        this.uptime = uptime;
//    }
//
//    public Boolean getIs_rolling_update() {
//        return is_rolling_update;
//    }
//
//    public void setIs_rolling_update(Boolean is_rolling_update) {
//        this.is_rolling_update = is_rolling_update;
//    }
//
//    public Long getFtime() {
//        return ftime;
//    }
//
//    public void setFtime(Long ftime) {
//        this.ftime = ftime;
//    }
//
//    public Double getA_liveness() {
//        return a_liveness;
//    }
//
//    public void setA_liveness(Double a_liveness) {
//        this.a_liveness = a_liveness;
//    }
//
//    public Double getA_influence() {
//        return a_influence;
//    }
//
//    public void setA_influence(Double a_influence) {
//        this.a_influence = a_influence;
//    }
//
//    public List<SocialUser.FollowersDistItem> getFollowers_dist() {
//        return followers_dist;
//    }
//
//    public void setFollowers_dist(List<SocialUser.FollowersDistItem> followers_dist) {
//        this.followers_dist = followers_dist;
//    }
//
//    public List<SocialUser.Friend> getFriends() {
//        return friends;
//    }
//
//    public void setFriends(List<SocialUser.Friend> friends) {
//        this.friends = friends;
//    }
//
//    public String getLocation_country() {
//        return location_country;
//    }
//
//    public void setLocation_country(String location_country) {
//        this.location_country = location_country;
//    }
//
//    public String getUtc_offset() {
//        return utc_offset;
//    }
//
//    public void setUtc_offset(String utc_offset) {
//        this.utc_offset = utc_offset;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getGender() {
//        return gender;
//    }
//
//    public void setGender(String gender) {
//        this.gender = gender;
//    }
//
//    public String getBirthday() {
//        return birthday;
//    }
//
//    public void setBirthday(String birthday) {
//        this.birthday = birthday;
//    }
//
//    public Short getBirthyear() {
//        return birthyear;
//    }
//
//    public void setBirthyear(Short birthyear) {
//        this.birthyear = birthyear;
//    }
//
//    public String getCollege() {
//        return college;
//    }
//
//    public void setCollege(String college) {
//        this.college = college;
//    }
//
//    public String getCity() {
//        return city;
//    }
//
//    public void setCity(String city) {
//        this.city = city;
//    }
//
//    public Short getPage_type() {
//        return page_type;
//    }
//
//    public void setPage_type(Short page_type) {
//        this.page_type = page_type;
//    }
//
//    public static class FollowersDistItem {
//        String update_date;
//        Long count;
//
//        public FollowersDistItem() {
//        }
//
//        public String getUpdate_date() {
//            return update_date;
//        }
//
//        public void setUpdate_date(String update_date) {
//            this.update_date = update_date;
//        }
//
//        public Long getCount() {
//            return count;
//        }
//
//        public void setCount(Long count) {
//            this.count = count;
//        }
//    }
//
//    public static class Friend {
//        String id;
//        String screen_name;
//        Boolean following;
//
//        public Friend() {
//        }
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getScreen_name() {
//            return screen_name;
//        }
//
//        public void setScreen_name(String screen_name) {
//            this.screen_name = screen_name;
//        }
//
//        public Boolean getFollowing() {
//            return following;
//        }
//
//        public void setFollowing(Boolean following) {
//            this.following = following;
//        }
//    }
    public void setFollowers_count(Long count){
        this.followers_count = count;
    }
}
