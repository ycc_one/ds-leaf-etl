package com.leaf.etl.es;

import com.leaf.etl.conf.EtlConsts;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author：ycc
 * @since： 2016/12/17
 */
public abstract class EsIml {
    protected TransportClient client;
    private static SimpleDateFormat basic = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    private static SimpleDateFormat basic1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.ENGLISH);
    private static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");

    protected EsIml() throws UnknownHostException {
        Settings settings = Settings.settingsBuilder()
                .put("cluster.name", EtlConsts.ES_CLUSTER)
                .put("client.transport.ping_timeout", "30s")
                .build();
        client = TransportClient.builder().settings(settings).build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("devrhino_es_1"), 9300))
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("devrhino_es_2"), 9300));

    }


    /**
     * wrap sourceMap to a Class object specialed
     *
     * @param sourceMap es search sourceMap
     * @param aClass    class type
     * @return a object
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ParseException
     */
    public static <T> T fromSourceMap(Map<String, Object> sourceMap, Class<?> aClass) throws IllegalAccessException, InstantiationException, ParseException, ClassNotFoundException {
        T object = (T) aClass.newInstance();
        Field[] fields = aClass.getDeclaredFields();
        for (Field field : fields) {
            if (sourceMap.containsKey(field.getName())) {
                field.setAccessible(true);
                switch (field.getType().getName()) {
                    case "java.lang.Long":
                        field.set(object, Long.valueOf(String.valueOf(sourceMap.get(field.getName()))));
                        break;
                    case "long":
                        field.set(object, Long.valueOf(String.valueOf(sourceMap.get(field.getName()))));
                        break;
                    case "java.lang.Short":
                        field.set(object, Short.valueOf(String.valueOf(sourceMap.get(field.getName()))));
                        break;
                    case "java.lang.Boolean":
                        field.set(object, Boolean.valueOf(String.valueOf(sourceMap.get(field.getName()))));
                        break;
                    case "java.lang.String":
                        field.set(object, sourceMap.get(field.getName()));
                        break;
                    case "java.lang.Double":
                        field.set(object, Double.parseDouble(String.valueOf(sourceMap.get(field.getName()))));
                        break;
                    case "java.util.Date":
                        field.set(object, fromBasic((String) sourceMap.get(field.getName())));
                        break;
                    case "java.lang.Integer":
                        field.set(object, Integer.valueOf(String.valueOf(sourceMap.get(field.getName()))));
                        break;
                    case "java.util.Map":
                        field.set(object, sourceMap.get(field.getName()));
                        break;
                    case "java.util.List":
                        Type type = field.getGenericType();
                        if (type instanceof ParameterizedType) {
                            ParameterizedType pType = (ParameterizedType) type;
                            Class<?> argType = (Class<?>) pType.getActualTypeArguments()[0];
                            field.set(object, fromSourceList((List<Object>) sourceMap.get(field.getName()), argType));
                        } else {
                            field.set(object, fromSourceList((List<Object>) sourceMap.get(field.getName()), field.getType()));
                        }
                        break;
                    default:
                        // 非原生类型 , 采用 fromSourceMap
                        field.set(object, fromSourceMap((Map<String, Object>) sourceMap.get(field.getName()), field.getType()));
                        break;
                }
            }
        }
        return object;
    }


    public static Date fromBasic(String date) {
        try {
            return basic.parse(date);
        } catch (Exception e) {
            try {
                return basic1.parse(date);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Just for source List
     *
     * @param properties list of sourceMap
     * @param aClass     class<?>
     * @return
     */
    private static List fromSourceList(List<Object> properties, Class<?> aClass) throws IllegalAccessException, ParseException, InstantiationException, ClassNotFoundException {
        if (properties == null) return null;
        List result = new ArrayList();
        for (Object property : properties) {
            if (property instanceof java.util.Map) {
                result.add(fromSourceMap((Map<String, Object>) property, aClass));
            } else {
                result.add(property);
            }

        }
        return result;
    }


}
