package com.leaf.etl.es;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author：ycc
 * @since： 2016/12/10
 */
public class EsDoc {
    private static final String FIELD_ID = "id";
    private String index_name= null;
    private String index_type = null;
    private String parent_id = null;

    private Map<String, Object> map = new HashMap<String, Object>();

    public EsDoc(String id, Map<String, Object> map ){
        this.map = map;
        this.map.put(FIELD_ID, id);
    }

    public EsDoc(Map<String, Object> map ){
        this.map = map;

    }

    public EsDoc(EsDoc esDoc){
        map.putAll(esDoc.map);
        //深拷贝，不能把id也拷贝过来
        map.remove(FIELD_ID);
    }

    public EsDoc(){
    }

    public EsDoc(String id ){
        map.put(FIELD_ID, id);
    }

    public String getId() {
        return (String) map.get(FIELD_ID);
    }

    public boolean containsKey(String key){
        return map.containsKey(key);
    }

    public Object get(String key){
        return map.get(key);
    }

    public void put(String key, Object value){
        map.put(key, value);
    }

    public Object remove(String key){
        return map.remove(key);
    }

    public int size(){ return map.size(); }

    public String toJson() throws IOException {
        return new Gson().toJson(map);
    }

    public String getIndex_name() {
        return index_name;
    }

    public void setIndex_name(String index_name) {
        this.index_name = index_name;
    }

    public String getIndex_type() {
        return index_type;
    }

    public void setIndex_type(String index_type) {
        this.index_type = index_type;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }
}
