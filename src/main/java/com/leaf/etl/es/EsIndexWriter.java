package com.leaf.etl.es;

import com.leaf.etl.conf.EtlConsts;
import com.leaf.etl.util.StringUtils;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequestBuilder;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.Requests;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.routing.allocation.decider.Decision;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * @author：ycc
 * @since： 2016/12/10
 */
public class EsIndexWriter extends EsIml {
    private static Map<String, EsIndexWriter> writerMap = null;
    public static final int MAX_BULK_NUM = 2000;
    public static EsIndexWriter esIndexWriter = null;
    public BulkProcessor bulkProcessor;

    public EsIndexWriter getInstance() throws UnknownHostException {
        if (esIndexWriter == null) {
            esIndexWriter = new EsIndexWriter();
        }
        return esIndexWriter;
    }

    private EsIndexWriter() throws UnknownHostException {
        super();
    }





    public Map<String, Object> get(String index_name, String index_type, String id) {
        GetResponse response = client.prepareGet(index_name, index_type, id).get();
        return response.getSource();
    }

    public boolean delete(EsDoc esDoc, String index_name, String index_type, String parentId) {
        DeleteRequest deleteRequest = Requests.deleteRequest(index_name)
                .type(index_type).id(esDoc.getId());
        if (!StringUtils.isEmpty(parentId)) {
            deleteRequest.parent(parentId);
        }
        RefreshRequest refreshRequest = Requests.refreshRequest(index_name);
        client.admin().indices().refresh(refreshRequest).actionGet();
        bulkProcessor.add(deleteRequest);
        return true;
    }

    public boolean delete(EsDoc esDoc) {
        DeleteResponse deleteResponse = client.prepareDelete().setRefresh(true)
                .setIndex(esDoc.getIndex_name())
                .setType(esDoc.getIndex_type())
                .setId(esDoc.getId())
                .get();
        return true;
    }

    public boolean update(EsDoc esDoc, Map<String, Object> updateMap) throws IOException, ExecutionException, InterruptedException {
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index(esDoc.getIndex_name());
        updateRequest.type(esDoc.getIndex_type());
        updateRequest.id(esDoc.getId());
        XContentBuilder builder = jsonBuilder().startObject();
        for (Map.Entry<String, Object> entry : updateMap.entrySet()) {
            builder.field(entry.getKey(), entry.getValue());
        }
        updateRequest.doc(builder.endObject());
        client.update(updateRequest).get();
        return true;
    }

    public List<Map<String, Object>> multiGet(List<EsDoc> docList) {
        MultiGetRequestBuilder multiGetRequestBuilder = client.prepareMultiGet();
        for (EsDoc esDoc : docList) {
            multiGetRequestBuilder.add(esDoc.getIndex_name(), esDoc.getIndex_type(), esDoc.getId());
        }
        List<Map<String, Object>> results = new LinkedList<Map<String, Object>>();
        for (MultiGetItemResponse itemResponse : multiGetRequestBuilder.get()) {
            results.add(itemResponse.getResponse().getSource());
        }
        return results;
    }


    public static void main(String[] args) throws UnknownHostException {
        EsIndexWriter esIndexWriter = new EsIndexWriter();
        esIndexWriter.get("797207837697069057", "", "");
    }


}
