package com.leaf.etl.conf;

import javax.security.auth.login.Configuration;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author：ycc
 * @since： 2016/12/11
 */
public class EtlConf {
    public Properties getConf() throws IOException {
        InputStream is = ClassLoader.getSystemResourceAsStream("etl.properties");
        Properties properties = new Properties();
        properties.load(is);
        return properties;
    }

    public static void main(String[] args) throws IOException {
        EtlConf etlConf= new EtlConf();
        System.out.println(etlConf.getConf().getProperty("es_hosts"));
    }
}
