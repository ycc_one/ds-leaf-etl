package com.leaf.etl.utilLearn.implement;

import java.util.Observable;

/**
 * @author：ycc
 * @since： 2016/12/19
 */
public class NumObserve extends Observable {
    private int num = 0;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
        setChanged();
        notifyObservers();
    }
}
