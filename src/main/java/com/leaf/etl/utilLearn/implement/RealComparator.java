package com.leaf.etl.utilLearn.implement;

import com.leaf.etl.entity.SocialUser;

import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

/**
 * @author：ycc
 * @since： 2016/12/18
 */
public class RealComparator implements Comparator<SocialUser> {

    @Override
    public int compare(SocialUser o1, SocialUser o2) {
        return 0;
    }

    @Override
    public Comparator<SocialUser> reversed() {
        return null;
    }

    @Override
    public Comparator<SocialUser> thenComparing(Comparator<? super SocialUser> other) {
        return null;
    }

    @Override
    public <U> Comparator<SocialUser> thenComparing(Function<? super SocialUser, ? extends U> keyExtractor, Comparator<? super U> keyComparator) {
        return null;
    }

    @Override
    public <U extends Comparable<? super U>> Comparator<SocialUser> thenComparing(Function<? super SocialUser, ? extends U> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<SocialUser> thenComparingInt(ToIntFunction<? super SocialUser> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<SocialUser> thenComparingLong(ToLongFunction<? super SocialUser> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<SocialUser> thenComparingDouble(ToDoubleFunction<? super SocialUser> keyExtractor) {
        return null;
    }
}
