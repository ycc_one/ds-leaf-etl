package com.leaf.etl.utilLearn.implement;

import com.leaf.etl.entity.SocialUser;

import java.util.Enumeration;

/**
 * @author：ycc
 * @since： 2016/12/18
 */
public class RealEnumration implements Enumeration<SocialUser> {
    @Override
    public boolean hasMoreElements() {
        return false;
    }

    @Override
    public SocialUser nextElement() {
        return null;
    }
}
