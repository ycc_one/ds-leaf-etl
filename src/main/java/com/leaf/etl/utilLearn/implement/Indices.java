package com.leaf.etl.utilLearn.implement;

/**
 * @author：ycc
 * @since： 2016/12/19
 * 自定义对map和collection排序
 *
 */
public class Indices implements Comparable <Indices>{

    public int begin;
    public int end;


    public Indices(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    @Override
    public int compareTo(Indices o) {
        return begin - o.begin;
    }
}
