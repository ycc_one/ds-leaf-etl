import com.leaf.etl.entity.SocialUser;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.LinkedList;
import java.util.List;

/**
 * @author：ycc
 * @since： 2016/12/17
 */
public class TypeTest {
    @Test
    public void ParameterizedTypeTest() throws IllegalAccessException, InstantiationException {
        SocialUser user = new SocialUser();
        Class aClass = user.getClass();
        Field[] fields = aClass.getDeclaredFields();
        Method[] method = aClass.getDeclaredMethods();
        for(Method method1 : method){
            System.out.println(method1.getName());
        }
        for (Field field : fields) {
            field.setAccessible(true);
            System.out.println(field.getGenericType());
            Type type = field.getGenericType();
            if (type instanceof ParameterizedType) {
                System.out.println("Params:" + field.getName());
            }else {
                System.out.println("name:"+field.getName());
            }
        }
    }

    @Test
    public void method() throws InvocationTargetException, IllegalAccessException {
        SocialUser user = new SocialUser();
        Class aclass = user.getClass();
        Method[] methods = aclass.getMethods();
        for(Method method : methods){
            System.out.println(method.getName());
            method.invoke(user, 123l);
            System.out.println();
        }
    }
}
