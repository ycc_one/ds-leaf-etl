import com.leaf.etl.entity.SocialUser;
import com.leaf.etl.utilLearn.implement.NumObServer;
import com.leaf.etl.utilLearn.implement.NumObserve;
import com.leaf.etl.utilLearn.implement.RealComparator;
import org.junit.Test;

import java.util.*;

/**
 * @author：ycc
 * @since： 2016/12/18
 */
public class CollectionTest {
    @Test
    public void iteratorTest() {
        List<String> list = new LinkedList<>();
        list.add("ycc");
        list.add("add");
        list.add("ttt");
        Iterator<String> stringIterator = list.iterator();

        String[] array = list.toArray(new String[list.size()]);
        while (stringIterator.hasNext()) {
            System.out.println(stringIterator.next());
        }
    }

    @Test
    public void comparatorTest() {
        Collections.sort(new LinkedList<SocialUser>(), new RealComparator());
    }

    @Test
    public void DequeTest() {
        LinkedList<String> list = new LinkedList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        ListIterator<String> iterator = list.listIterator();
        list.element();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());

        }
    }

    @Test
    public void formatter() {
        Formatter formatter = new Formatter();
    }

    @Test
    public void observeTest() {
        NumObserve observe = new NumObserve();
//        NumObServer obServer = new NumObServer();
        observe.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                NumObserve numObserve = (NumObserve) o;
//                System.out.println(numObserve.getNum());
                if (numObserve.getNum() % 2 == 1) {
                    System.out.println("test Observe:" + numObserve.getNum());
                }
            }
        });

        for (int i = 0; i<20; i++){
            observe.setNum(i);
        }

    }

}
